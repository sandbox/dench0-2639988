<?php

/**
 * @file
 * Contains \Drupal\ml_inaccessible_visible\InaccessibleVisibleMenuInterface.
 */

namespace Drupal\ml_inaccessible_visible;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a menu entity.
 */
interface InaccessibleVisibleMenuInterface extends ConfigEntityInterface {


}
