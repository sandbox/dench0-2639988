<?php

/**
 * @file
 * Contains \Drupal\ml_inaccessible_visible\Form\MlInaccessibleVisibleSettingsFrom.
 */

namespace Drupal\ml_inaccessible_visible\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;


/**
 * Configure Youtube settings for this site.
 */
class MlInaccessibleVisibleSettingsFrom extends FormBase {

  /**
   * The menu storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $menuStorage;

  /**
   * The menu storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $ivMenuStorage;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;


  /**
   * Constructs new SystemMenuBlock.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $menu_storage
   *   The menu storage.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(EntityStorageInterface $menu_storage, EntityStorageInterface $iv_menu_storage, ModuleHandlerInterface $module_handler, BlockManagerInterface $block_manager) {
    $this->menuStorage = $menu_storage;
    $this->ivMenuStorage = $iv_menu_storage;
    $this->moduleHandler = $module_handler;
    $this->blockManager = $block_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('menu'),
      $container->get('entity.manager')->getStorage('inaccessible_visible_menu'),
      $container->get('module_handler'),
      $container->get('plugin.manager.block')
    );
    }

    /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'ml_inaccessible_visible_settings_form';
  }

  /**
   * Gets the menus to display in this form.
   *
   * @return \Drupal\system\Entity\Menu[]
   *   An array of menu objects.
   */
  protected function getMenus() {
    return $this->menuStorage->loadMultiple();
  }

  /**
   * Gets the menus to display in this form.
   *
   * @return \Drupal\system\Entity\Menu[]
   *   An array of menu objects.
   */
  protected function getIvMenus() {
    return $this->ivMenuStorage->loadMultiple();
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    foreach ($this->getMenus() as $menu => $entity) {
      $options[$entity->id()] = $entity->label();
    }
    $default_value = array();
    foreach ($this->getIvMenus() as $menu => $entity) {
      $default_value[$entity->id()] = $entity->id();
    }
    $form['menus'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => t('In which menus need to always show inaccessible links?'),
      '#description' => t('When you select menus - new blocks with suffix "Inaccessible links" will be appear. You can place this block in any regions and Inaccessible Links in this block does not disappear'),
      '#default_value' => $default_value,
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save settings'),
      '#button_type' => 'primary',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $submitted_menus = array_filter($values['menus']);
    $menus = $this->getMenus();
    $iv_menus = $this->getIvMenus();
    $delete_items = array_diff_key($iv_menus, $submitted_menus);
    $this->ivMenuStorage->delete($delete_items);
    $insert_items = array_diff_key($submitted_menus, $iv_menus);
    foreach($insert_items as $menu) {
      $entity = $this->ivMenuStorage->create([
          'id' => $menus[$menu]->id(),
          'label' => $menus[$menu]->label() . ' Inaccessible Visible',
          'menu_dependency' => $menus[$menu]->getConfigDependencyName(),
        ]
      );
      $this->ivMenuStorage->save($entity);
    }
    if ($this->moduleHandler->moduleExists('block')) {
      $this->blockManager->clearCachedDefinitions();
    }
    drupal_set_message($this->t('The changes have been saved.'));
  }

}
