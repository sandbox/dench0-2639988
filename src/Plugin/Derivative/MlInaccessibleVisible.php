<?php

/**
 * @file
 * Contains \Drupal\ml_inaccessible_visible\Plugin\Derivative\MlInaccessibleVisible.
 */

namespace Drupal\ml_inaccessible_visible\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for Inaccessible Visible menus.
 *
 * @see \Drupal\ml_inaccessible_visible\Plugin\Block\MlInaccessibleVisible
 */
class MlInaccessibleVisible extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The menu storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $menuStorage;

  /**
   * The menu storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $ivMenuStorage;

  /**
   * Constructs new SystemMenuBlock.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $menu_storage
   *   The menu storage.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(EntityStorageInterface $menu_storage, EntityStorageInterface $iv_menu_storage) {
    $this->menuStorage = $menu_storage;
    $this->ivMenuStorage = $iv_menu_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity.manager')->getStorage('menu'),
      $container->get('entity.manager')->getStorage('inaccessible_visible_menu')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $menus = $this->menuStorage->loadMultiple();
    $iv_menus = $this->ivMenuStorage->loadMultiple();
    foreach ($iv_menus as $iv_menu => $entity) {
      if (isset($menus[$iv_menu])) {
        $this->derivatives[$iv_menu] = $base_plugin_definition;
        $this->derivatives[$iv_menu]['admin_label'] = $entity->label();
        $this->derivatives[$iv_menu]['config_dependencies']['config'] = [
          $menus[$iv_menu]->getConfigDependencyName(),
          $entity->getConfigDependencyName(),
        ];
      }
    }
    return $this->derivatives;
  }
}
