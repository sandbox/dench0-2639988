<?php

/**
 * @file
 * Contains \Drupal\ml_inaccessible_visible\Entity\InaccessibleVisibleMenu.
 */

namespace Drupal\ml_inaccessible_visible\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\ml_inaccessible_visible\InaccessibleVisibleMenuInterface;

/**
 * Defines the Menu configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "inaccessible_visible_menu",
 *   label = @Translation("Inaccessible Visible Menu Settings"),
 *   admin_permission = "administer menu",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   }
 * )
 */

class InaccessibleVisibleMenu extends ConfigEntityBase implements InaccessibleVisibleMenuInterface {

  /**
   * The menu machine name.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the menu entity.
   *
   * @var string
   */
  protected $label;

  /**
   * The human-readable name of the menu entity.
   *
   * @var string
   */
  protected $menu_dependency = NULL;

  /**
   * {@inheritdoc}
   */

  public function calculateDependencies() {
    parent::calculateDependencies();
    if (!empty($this->menu_dependency)) {
      $this->addDependency('config', $this->menu_dependency);
    }
    return $this;
  }
}
